package com.experiments.model;

public class BasicJsonProperty {
	private String propertyName;
	private String type;
	private String example;

	public BasicJsonProperty() {
		;
	}

	public BasicJsonProperty(String propertyName, String type, String example) {
		super();
		this.propertyName = propertyName;
		this.type = type;
		this.example = example;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	@Override
	public String toString() {
		return String.format("StringOrBooleanJsonProperty [propertyName=%s, type=%s, example=%s]", propertyName, type,
				example);
	}

}
