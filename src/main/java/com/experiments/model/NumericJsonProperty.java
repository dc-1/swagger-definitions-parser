package com.experiments.model;

public class NumericJsonProperty extends BasicJsonProperty {

	private String format;

	public NumericJsonProperty() {
		;
	}

	public NumericJsonProperty(String propertyName, String type, String format, String example) {
		super(propertyName, type, example);
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return String.format("NumericJsonProperty [propertyName=%s, type=%s, example=%s, format=%s]",
				this.getPropertyName(), this.getType(), this.getExample(), format);
	}

}
