package com.experiments.model;

public class ArrayJsonProperty extends BasicJsonProperty {
	private BasicJsonProperty arrayItem;

	public ArrayJsonProperty() {
	}

	public ArrayJsonProperty(String propertyName, String type, String example) {
		super(propertyName, type, example);
	}

	public BasicJsonProperty getArrayItem() {
		return arrayItem;
	}

	public void setArrayItem(BasicJsonProperty arrayItem) {
		this.arrayItem = arrayItem;
	}

	@Override
	public String toString() {
		return String.format("ArrayJsonProperty [arrayItem=%s]", arrayItem);
	}

}
