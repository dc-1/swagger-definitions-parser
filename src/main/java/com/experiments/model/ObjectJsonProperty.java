package com.experiments.model;

import java.util.ArrayList;
import java.util.List;

public class ObjectJsonProperty extends BasicJsonProperty {

	private List<? super BasicJsonProperty> properties = new ArrayList<>();

	public ObjectJsonProperty() {
	}

	public ObjectJsonProperty(String propertyName, String type, String example) {
		super(propertyName, type, example);
	}

	public List<? super BasicJsonProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<? super BasicJsonProperty> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		return String.format("ObjectJsonProperty [propertyName=%s, type=%s, properties=%s]", getPropertyName(),
				getType(), properties);
	}

}
