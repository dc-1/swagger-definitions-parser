package com.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import com.experiments.model.ArrayJsonProperty;
import com.experiments.model.BasicJsonProperty;
import com.experiments.model.NumericJsonProperty;
import com.experiments.model.ObjectJsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SwaggerDefinitionsParser {
	
	// Keys that are reserved from a Swagger definition perspective.
	private List<String> reservedKeys = Arrays.asList("type", "format", "example", "items");

	// Flattened JSON properties. 
	// Trio of {TYPE, FORMAT and EXAMPLE} mapped against the json-path.
	private Map<String, String[]> jsonPaths = new TreeMap<>();

	// Stack that stores the parental prefix while parsing through the object graph.
	private Stack<String> parents = new Stack<>();

	public void parse(String classpathResource) throws JsonParseException, JsonMappingException, IOException {
		// Read the Swagger Definition JSON file.
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(classpathResource);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(stream, new TypeReference<Map<String, Object>>() {
		});
		
		// Convert Swagger Definitions into their Java Model Counterparts.
		ObjectJsonProperty result = new ObjectJsonProperty();
		parseJsonProp(jsonMap, Strings.EMPTY, result);

		// Parse the Java Model and create a Map<String, String[]>
		// json-path -> {TYPE, FORMAT and EXAMPLE}
		flattenIt(result);

		prettyPrintJsonPaths();
	}

	private void prettyPrintJsonPaths() {
		System.out.println(String.format("%-60s\t%-10s\t%-10s\t%-100s", "PATH", "TYPE", "FORMAT", "EXAMPLE"));

		for (String path : jsonPaths.keySet()) {

			String[] values = jsonPaths.get(path);
			if (parents.isEmpty()) {
				System.out.println(String.format("%-60s\t%-10s\t%-10s\t%-100s", path, values[0], values[1], values[2]));
				parents.add(path + ".");
			} else {
				boolean matchingParentAvailable = false;
				while (!(matchingParentAvailable = path.startsWith(parents.peek()))) {
					parents.pop();
				}
				if (matchingParentAvailable) {
					String parentalPrefix = parents.peek();
					String trimmedProperty = path.replaceFirst(parentalPrefix, " ".repeat(parentalPrefix.length()));
					System.out.println(String.format("%-60s\t%-10s\t%-10s\t%-100s", trimmedProperty, values[0],
							values[1], values[2]));
					parents.add(path + ".");
				}
			}
		}
	}

	private void flattenIt(Object result) {
		if (result != null) {
			if (result instanceof ObjectJsonProperty) {
				ObjectJsonProperty ojp = (ObjectJsonProperty) result;
				jsonPaths.put(ojp.getPropertyName(), new String[] { ojp.getType(), Strings.EMPTY, Strings.EMPTY });
				for (Object prop : ojp.getProperties()) {
					flattenIt(prop);
				}
			} else if (result instanceof ArrayJsonProperty) {
				ArrayJsonProperty ajp = (ArrayJsonProperty) result;
				jsonPaths.put(ajp.getPropertyName(), new String[] { ajp.getType(), Strings.EMPTY, ajp.getExample() });
				flattenIt(ajp.getArrayItem());
			} else if (result instanceof NumericJsonProperty) {
				NumericJsonProperty njp = (NumericJsonProperty) result;
				jsonPaths.put(njp.getPropertyName(), new String[] { njp.getType(), njp.getFormat(), njp.getExample() });
			} else if (result instanceof BasicJsonProperty) {
				BasicJsonProperty bjp = (BasicJsonProperty) result;
				jsonPaths.put(bjp.getPropertyName(), new String[] { bjp.getType(), Strings.EMPTY, bjp.getExample() });
			}

		}
	}

	@SuppressWarnings("unchecked")
	private void parseJsonProp(Map<String, Object> map, String parentProperty, ObjectJsonProperty result) {

		List<String> filteredKeys = map.keySet().stream().filter(k -> !reservedKeys.contains(k))
				.collect(Collectors.toList());
		for (String key : filteredKeys) {
			boolean nonRootLevelProperties = parentProperty != null && !parentProperty.isEmpty();
			String currentProperty = nonRootLevelProperties ? parentProperty + "." + key : key;
			System.out.println("Processing key : " + key);

			Object innerMap = map.get(key);
			System.out.println("innerMap : " + innerMap);

			Object typeInstance = ((Map<String, Object>) innerMap).get("type");
			if (typeInstance != null && !(typeInstance.getClass().getSimpleName().equalsIgnoreCase("string"))) {
				System.out.println("Looks like a custom property having name 'type'.");
				typeInstance = null;
			}
			String type = (String) typeInstance;
			System.out.println("type : " + type);

			if (type == null) {
				result.setPropertyName(currentProperty);
				result.setType("object");
				parseJsonProp((Map<String, Object>) innerMap, currentProperty, result);
			} else if (type.equalsIgnoreCase("object")) {
				ObjectJsonProperty ojp = new ObjectJsonProperty();
				ojp.setPropertyName(currentProperty);
				ojp.setType("object");
				result.getProperties().add(ojp);
				parseJsonProp((Map<String, Object>) ((Map<String, Object>) innerMap).get("properties"), currentProperty,
						ojp);
			} else if ("string".equalsIgnoreCase(type) || "boolean".equalsIgnoreCase(type)) {
				result.getProperties().add(new BasicJsonProperty(currentProperty, type,
						(String) ((Map<String, Object>) innerMap).get("example")));
			} else if ("integer".equalsIgnoreCase(type) || "number".equalsIgnoreCase(type)) {
				result.getProperties()
						.add(new NumericJsonProperty(currentProperty, type,
								(String) ((Map<String, Object>) innerMap).get("format"),
								(String) ((Map<String, Object>) innerMap).get("example")));
			} else if ("array".equalsIgnoreCase(type)) {
				ArrayJsonProperty ajp = new ArrayJsonProperty();
				ajp.setPropertyName(currentProperty);
				ajp.setType("array");
				result.getProperties().add(ajp);
				ObjectJsonProperty arrayItem = new ObjectJsonProperty("items", "object", "");
				Map<String, Object> arrayInnerMap = (Map<String, Object>) ((Map<String, Object>) innerMap).get("items");
				parseJsonProp(arrayInnerMap, currentProperty, arrayItem);
				ajp.setArrayItem(arrayItem);

			}
		}

	}

}
