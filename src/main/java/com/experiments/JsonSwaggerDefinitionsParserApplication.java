package com.experiments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonSwaggerDefinitionsParserApplication implements CommandLineRunner {

	@Autowired
	private SwaggerDefinitionsParser parser;

	public static void main(String[] args) {
		SpringApplication.run(JsonSwaggerDefinitionsParserApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		parser.parse("repos/get_all_repos_swagger_definitions.json");
		//comment
	}

}
